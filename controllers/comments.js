var comment = require('../models/comments');

exports.readComments = function(req, res){
    comments.find({})
    .then(function(comments){
        res.json(comments);
    })
    .catch(function(error){
        res.json(error);
    });
};

exports.readOneComment = function(req, res){
    console.log('Updating comments with ID' + req.params.id);
    comments.findById(req.params.id)
    .then(function(comments){
        res.json(comments);
    })
    .catch(function(error){
        console.error('Error updating comments with ID' + req.params.id, error);
        res.json(error);
    })
};

exports.createComment = function(req, res) {
    var newcomments = new comments(req.body);
    newcomments.save()
    .then(function(comments){
        res.json(comments);
    })
    .catch(function(error){
        res.json(error)
    });
};

exports.updateOneComment = function(req, res){
    var id = req.params.id;
    var options = {
        new: true
    };
    comments.findByIdAndUpdate(id, req.body,options)
    .then(function(comments){
        res.json(comments);
    })
    .catch(function(error){
        console.error('Error updating comments with ID' + req.params.id, error);
        res.json(error);
    });
};

exports.deleteOneComment = function(req, res){
    var id = req.params.id;
    comments.findByIdAndRemove(id)
    .then(function(){
        res.json({});
    })
    .catch(function(error){
        console.error('Error deleting comments with ID' + req.params.id, error);
        res.json(error);
    });
};
